﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ztrader
{
    public enum SessionBreak { AfternoonClose, EveningOpen, MorningOpen, NextDay };

    public class Utils
    {
        public const string WEBROOTPATH = @"C:\inetpub\wwwroot\nt_files\";

        public static string GetTimeDate(String str_timedate, int time_date)
        {
            char[] delimiterChars = { ' ' };
            string[] str_arr = str_timedate.Split(delimiterChars);
            return str_arr[time_date];
        }

        public static string GetTimeDate(DateTime dt, int time_date)
        {
            string str_dt = dt.ToString("MMddyyyy HH:mm:ss");
            char[] delimiterChars = { ' ' };
            string[] str_arr = str_dt.Split(delimiterChars);
            return str_arr[time_date];
        }

        public static double GetTimeDiff(DateTime dt_st, DateTime dt_en)
        {
            double diff = -1;
            //if(diff < 0 ) return 100; 
            try
            {

                if ((int)dt_st.DayOfWeek == (int)dt_en.DayOfWeek)
                { //Same day
                    if (CompareTimeWithSessionBreak(dt_st, SessionBreak.AfternoonClose) * CompareTimeWithSessionBreak(dt_en, SessionBreak.AfternoonClose) > 0)
                    {
                        diff = dt_en.Subtract(dt_st).TotalMinutes;
                    }
                    else if (CompareTimeWithSessionBreak(dt_st, SessionBreak.AfternoonClose) < 0 && CompareTimeWithSessionBreak(dt_en, SessionBreak.AfternoonClose) > 0)
                    {
                        diff = GetTimeDiffSession(dt_st, SessionBreak.AfternoonClose) + GetTimeDiffSession(dt_en, SessionBreak.EveningOpen);
                    }
                }
                else if ((dt_st.DayOfWeek == DayOfWeek.Friday && dt_en.DayOfWeek == DayOfWeek.Sunday) || ((int)dt_st.DayOfWeek > (int)dt_en.DayOfWeek) || ((int)dt_st.DayOfWeek < (int)dt_en.DayOfWeek - 1))
                { // Fiday - Sunday or Cross to next Week or have day off trade
                    diff = GetTimeDiffSession(dt_st, SessionBreak.AfternoonClose) + GetTimeDiffSession(dt_en, SessionBreak.EveningOpen);
                }
                else if ((int)dt_st.DayOfWeek == (int)dt_en.DayOfWeek - 1)
                { //Same day or next day
                    if (CompareTimeWithSessionBreak(dt_st, SessionBreak.AfternoonClose) < 0)
                    {
                        diff = GetTimeDiffSession(dt_st, SessionBreak.AfternoonClose) + GetTimeDiffSession(dt_en, SessionBreak.NextDay);
                    }
                    else if (CompareTimeWithSessionBreak(dt_st, SessionBreak.AfternoonClose) > 0)
                    { // dt_st passed evening open, no need to adjust
                        diff = diff = dt_en.Subtract(dt_st).TotalMinutes;
                    }
                }
                else
                {
                    diff = dt_en.Subtract(dt_st).TotalMinutes;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTimeDiff ex:" + dt_st.ToString() + "--" + dt_en.ToString() + "--" + ex.Message);
                diff = 100;
            }
            return Math.Round(diff, 2);
        }

        public static double GetMinutesDiff(DateTime dt_st, DateTime dt_en)
        {
            double diff = -1;
            TimeSpan ts = dt_en.Subtract(dt_st);
            diff = ts.TotalMinutes;
            return Math.Round(diff, 2);
        }

        public static int CompareTimeWithSessionBreak(DateTime dt_st, SessionBreak sb)
        {
            DateTime dt = DateTime.Now;
            try
            {
                switch (sb)
                {
                    case SessionBreak.AfternoonClose:
                        dt = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day, 16, 0, 0);
                        break;
                    case SessionBreak.EveningOpen:
                        if (dt_st.Hour < 16)
                            dt = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day - 1, 17, 0, 0);
                        //if(dt_st.Hour >= 17)
                        else dt = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day, 16, 0, 0);
                        break;
                    default:
                        dt = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day, 16, 0, 0);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CompareTimeWithSessionBreak ex:" + dt_st.ToString() + "--" + sb.ToString() + "--" + ex.Message);
            }
            return dt_st.CompareTo(dt);
        }

        public static double GetTimeDiffSession(DateTime dt_st, SessionBreak sb)
        {
            DateTime dt_session = DateTime.Now;
            TimeSpan ts = dt_session.Subtract(dt_st);
            double diff = 100;
            try
            {
                switch (sb)
                {
                    case SessionBreak.AfternoonClose:
                        dt_session = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day, 16, 0, 0);
                        ts = dt_session.Subtract(dt_st);
                        break;
                    case SessionBreak.EveningOpen:
                        if (dt_st.Hour < 16)
                            dt_session = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day - 1, 17, 0, 0);
                        else
                            dt_session = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day, 17, 0, 0);
                        ts = dt_st.Subtract(dt_session);
                        break;
                    case SessionBreak.NextDay:
                        dt_session = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day - 1, 17, 0, 0);
                        ts = dt_st.Subtract(dt_session);
                        break;
                    default:
                        dt_session = GetNewDateTime(dt_st.Year, dt_st.Month, dt_st.Day - 1, 17, 0, 0);
                        ts = dt_st.Subtract(dt_session);
                        break;
                }
                diff = ts.TotalMinutes;
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTimeDiffSession ex:" + dt_st.ToString() + "--" + sb.ToString() + "--" + ex.Message);
            }

            return Math.Round(diff, 2);
        }

        public static DateTime GetNewDateTime(int year, int month, int day, int hr, int min, int sec)
        {
            //DateTime(dt_st.Year,dt_st.Month,dt_st.Day,16,0,0);
            if (day == 0)
            {
                if (month == 1)
                {
                    year = year - 1;
                    month = 12;
                    day = 31;
                }
                else if (month == 3)
                {
                    month = 2;
                    day = 28;
                }
                else
                {
                    month--;
                    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10)
                        day = 31;
                    else day = 30;
                }
            }
            return new DateTime(year, month, day, hr, min, sec);
        }

        public static String GetTsTAccName(String tst_acc)
        {
            char[] delimiterChars = { '!' };
            string[] words = tst_acc.Split(delimiterChars);
            return words[0];
        }

        public static void FileTest(int barNo)
        {
            if (barNo < 0) return;
            FileStream F = new FileStream(@"C:\inetpub\wwwroot\nt_files\test\" + barNo.ToString() + ".dat", FileMode.OpenOrCreate,
               FileAccess.ReadWrite);

            for (int i = 1; i <= 20; i++)
            {
                F.WriteByte((byte)i);
            }
            F.Position = 0;
            for (int i = 0; i <= 20; i++)
            {
                Console.WriteLine(F.ReadByte() + " ");
            }
            F.Close();
            //Console.ReadKey();
        }
 	

        public static string GetFileNameByDateTime(DateTime dt, string path, string accName, string ext) {
			Console.WriteLine("GetFileNameByDateTime: " + dt.ToString());
			//path = "C:\\inetpub\\wwwroot\\nt_files\\log\\";
			//ext = "log";
			long flong = DateTime.Now.Minute + 100*DateTime.Now.Hour+ 10000*DateTime.Now.Day + 1000000*DateTime.Now.Month + (long)100000000*DateTime.Now.Year;
			string fname = path + accName + Path.DirectorySeparatorChar + accName + "_" + flong.ToString() + "." + ext;
			Console.WriteLine(", FileName=" + fname);
			//FileTest(DateTime.Now.Minute + 100*DateTime.Now.Hour+ 10000*DateTime.Now.Day+ 1000000*DateTime.Now.Month + (long)100000000*DateTime.Now.Year);

		 	//if(barNo > 0) return;
//			FileStream F = new FileStream(fname, FileMode.OpenOrCreate, FileAccess.ReadWrite);
			
			//using (System.IO.StreamWriter file = 
			//	new System.IO.StreamWriter(@fname, true))
			//{
			//	for (int i = 0; i <= 20; i++) {
			//		file.WriteLine("Line " + i + ":" + i);
			//	}
			//}
			return fname;
		}
		
		public static FileInfo[] GetCmdFile(string srcDir) {
			Console.WriteLine("GetCmdFile src: " + srcDir);
		    DirectoryInfo DirInfo = new DirectoryInfo(srcDir);

//            var filesInOrder = from f in DirInfo.EnumerateFiles()
//                               orderbydescending f.CreationTime
//                               select f;
			
//			var filesInOrder = DirInfo.GetFiles("*.*",SearchOption.AllDirectories).OrderBy(f => f.LastWriteTime)
//								.ToList();
			//DirectoryInfo dir = new DirectoryInfo (folderpath);

			FileInfo[] filesInOrder = DirInfo.GetFiles().OrderByDescending(p => p.LastWriteTime).ToArray();
			
            foreach (FileInfo item in filesInOrder)
            {
                Console.WriteLine("cmdFile=" + item.FullName);
            }
			
			return filesInOrder;
		}
		
		public static void MoveCmdFiles(FileInfo[] src, string dest) {
			Console.WriteLine("MoveCmdFile src,dest: " + src.Length + "," + dest);
			foreach (FileInfo item in src)
            {
				string destFile = dest+item.Name;
				if (File.Exists(destFile))
				{
					File.Delete(destFile);
				}
				item.MoveTo(destFile);
				//File.Move(src, dest);
			}
		}
		
		public static Dictionary<string,string> ReadParaFile(FileInfo src) {
			Dictionary<string,string> paraMap = new Dictionary<string,string>();
			
			Console.WriteLine("ReadParaFile src: " + src);
			if (!src.Exists)
			{
				return paraMap;
			}
	
			int counter = 0;  
			string line;

			// Read the file and display it line by line.  
			System.IO.StreamReader file =   
				new System.IO.StreamReader(src.FullName);//@"c:\test.txt");
			while((line = file.ReadLine()) != null)  
			{
				string[] pa = line.Split(':');
				paraMap.Add(pa[0], pa[1]);
				Console.WriteLine(line);  
				counter++;
			}

			file.Close();
			Console.WriteLine("There were {0} lines." + counter);
			// Suspend the screen.
			//System.Console.ReadLine();
			return paraMap;
		}

        public static void PutCmdFile(Dictionary<string, string> paraObj, string fpath, string text)
        {
            Console.WriteLine("PrintLog: " + fpath);
           
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@fpath, true))
            {
                string val;
                
                foreach (string key in paraObj.Keys) {
                    paraObj.TryGetValue(key, out val);
                    file.WriteLine(key + ":" + val);
                }
            }
        }

        public static void PrintLog(bool pntcon, string fpath, string text) {
			Console.WriteLine("PrintLog: " + fpath);
			if(pntcon) Console.WriteLine(text); // return;
			using (System.IO.StreamWriter file = 
				new System.IO.StreamWriter(@fpath, true))
			{
				file.WriteLine(text);
			}
		}	

    }
}