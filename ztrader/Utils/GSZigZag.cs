﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ztrader
{
    public class GSZigZag
    {
        protected void LoadCmdFile()
        {
            FileInfo[] cmdFiles = Utils.GetCmdFile(@"C:\inetpub\wwwroot\nt_files\cmd\" + AccName + Path.DirectorySeparatorChar);
            if (cmdFiles.Length > 0)
            {
                Dictionary<string, string> dictParam = Utils.ReadParaFile(cmdFiles[0]);
                Utils.MoveCmdFiles(cmdFiles, @"C:\inetpub\wwwroot\nt_files\cmd_bak\" + AccName + Path.DirectorySeparatorChar);
                Type t = typeof(GSZigZag);
                foreach (string pa in dictParam.Keys)
                {
                    string val;
                    dictParam.TryGetValue(pa, out val);

                    PropertyInfo pi = t.GetProperty(pa);
                    Console.WriteLine("key,val, type=" + pa + "," + val + "," + pi.PropertyType.Name);
                    if (pi.PropertyType == typeof(System.Int32))
                    {
                        Console.WriteLine("Int32 detected");
                        int ival = System.Int32.Parse(val);
                        pi.SetValue(this, ival, null);
                    }
                    if (pi.PropertyType == typeof(System.Double))
                    {
                        Console.WriteLine("Double detected");
                        double dval = System.Double.Parse(val);
                        pi.SetValue(this, dval, null);
                        Console.WriteLine("ProfitTargetAmt=" + ProfitTargetAmt + ", dval=" + dval);
                    }
                    if (pi.PropertyType == typeof(System.Boolean))
                    {
                        Console.WriteLine("Boolean detected");
                        bool bval = System.Boolean.Parse(val);
                        pi.SetValue(this, bval, null);
                    }
                }
            }
        }

        #region Variables

        //command to change mode 0=liquidate; 1=start/resume trading; 2=cancel order; -1=stop trading;
        protected int algo_mode = 1;

        protected double retracePnts = 6; // Default setting for RetracePnts

        protected double profitTargetAmt = 350; //36 Default(450-650 USD) setting for ProfitTargetAmt
        protected double profitTgtIncTic = 6; //8 Default tick Amt for ProfitTarget increase Amt
        protected double profitLockMinTic = 16; //24 Default ticks Amt for Min Profit locking
        protected double profitLockMaxTic = 30; //80 Default ticks Amt for Max Profit locking
        protected double stopLossAmt = 200; //16 Default setting for StopLossAmt
        protected double stopLossIncTic = 4; //4 Default tick Amt for StopLoss increase Amt
        protected double breakEvenAmt = 150; //150 the profits amount to trigger setting breakeven order
        protected double trailingSLAmt = 100; //300 Default setting for trailing Stop Loss Amt
        protected double dailyLossLmt = -200; //-300 the daily loss limit amount

        protected int timeStart = 90100; //93300 Default setting for TimeStart
        protected int timeEnd = 105900; // Default setting for TimeEnd
        protected int minutesChkEnOrder = 10; //how long before checking an entry order filled or not
        protected int minutesChkPnL = 30; //how long before checking P&L

        protected int barsHoldEnOrd = 2; // Bars count since en order was issued
        protected int barsSincePtSl = 1; // Bar count since last P&L was filled
        protected int barsToCheckPL = 2; // Bar count to check P&L since the entry
                                         //protected int barsPullback = 1; // Bars count for pullback
        protected double enSwingMinPnts = 11; //10 Default setting for EnSwingMinPnts
        protected double enSwingMaxPnts = 16; //16 Default setting for EnSwingMaxPnts
        protected double enPullbackMinPnts = 5; //6 Default setting for EnPullbackMinPnts
        protected double enPullbackMaxPnts = 9; //10 Default setting for EnPullbackMaxPnts
        protected double enOffsetPnts = 0.5;//Price offset for entry
                                            //protected double enOffset2Pnts = 0.5;//Price offset for entry
        protected int enCounterPBBars = 1;//Bar count of pullback for breakout entry setup
        protected double enResistPrc = 2700; // Resistance price for entry order
        protected double enSupportPrc = 2600; // Support price for entry order

        protected bool enTrailing = true; //use trailing entry: counter pullback bars or simple enOffsetPnts
        protected bool ptTrailing = true; //use trailing profit target every bar
        protected bool slTrailing = true; //use trailing stop loss every bar
        protected bool resistTrailing = false; //track resistance price for entry order
        protected bool supportTrailing = false; //track resistance price for entry order

        protected int tradeDirection = 0; // -1=short; 0-both; 1=long;
        protected int tradeStyle = 1; // -1=counter trend; 1=trend following;
        protected bool backTest = false; //if it runs for backtesting;

        protected int printOut = 1; //0,1,2,3 more print
        protected bool drawTxt = false; // User defined variables (add any user defined variables below)
        //protected IText it_gap = null; //the Text draw for gap on current bar
        protected string log_file = ""; //

        //protected IOrder entryOrder = null;
        //protected IOrder profitTargetOrder = null;
        //protected IOrder stopLossOrder = null;
        protected double trailingPTTic = 36; //400, tick amount of trailing target
        protected double trailingSLTic = 16; // 200, tick amount of trailing stop loss
        protected int barsSinceEnOrd = 0; // bar count since the en order issued

        protected string AccName = null;

        //protected IDataSeries zzHighValue;
        //protected IDataSeries zzLowValue;
        //protected DataSeries zigZagSizeSeries;
        //protected DataSeries zigZagSizeZigZag;
        //protected Dictionary<int, IText> dictZZText;

        //protected double[]			lastZZs; // the ZZ prior to cur bar
        //protected ZigZagSwing[] latestZZs;
        protected string zzEntrySignal = "ZZEntry";

        protected int ZZ_Count_0_6 = 0;
        protected int ZZ_Count_6_10 = 0;
        protected int ZZ_Count_10_16 = 0;
        protected int ZZ_Count_16_22 = 0;
        protected int ZZ_Count_22_30 = 0;
        protected int ZZ_Count_30_ = 0;
        protected int ZZ_Count = 0;
        #endregion

        #region Properties

        public int AlgoMode
        {
            get { return algo_mode; }
            set { algo_mode = value; }
        }

        public double RetracePnts
        {
            get { return retracePnts; }
            set { retracePnts = Math.Max(1, value); }
        }

        public double ProfitTargetAmt
        {
            get { return profitTargetAmt; }
            set { profitTargetAmt = Math.Max(0, value); }
        }

        public double ProfitTgtIncTic
        {
            get { return profitTgtIncTic; }
            set { profitTgtIncTic = Math.Max(0, value); }
        }

        public double ProfitLockMinTic
        {
            get { return profitLockMinTic; }
            set { profitLockMinTic = Math.Max(0, value); }
        }

        public double ProfitLockMaxTic
        {
            get { return profitLockMaxTic; }
            set { profitLockMaxTic = Math.Max(0, value); }
        }


        public double StopLossAmt
        {
            get { return stopLossAmt; }
            set { stopLossAmt = Math.Max(0, value); }
        }


        public double TrailingStopLossAmt
        {
            get { return trailingSLAmt; }
            set { trailingSLAmt = Math.Max(0, value); }
        }


        public double StopLossIncTic
        {
            get { return stopLossIncTic; }
            set { stopLossIncTic = Math.Max(0, value); }
        }


        public double BreakEvenAmt
        {
            get { return breakEvenAmt; }
            set { breakEvenAmt = Math.Max(0, value); }
        }


        public double DailyLossLmt
        {
            get { return dailyLossLmt; }
            set { dailyLossLmt = Math.Min(-100, value); }
        }


        public int TimeStart
        {
            get { return timeStart; }
            set { timeStart = Math.Max(0, value); }
        }


        public int TimeEnd
        {
            get { return timeEnd; }
            set { timeEnd = Math.Max(0, value); }
        }


        public int MinutesChkEnOrder
        {
            get { return minutesChkEnOrder; }
            set { minutesChkEnOrder = Math.Max(0, value); }
        }


        public int MinutesChkPnL
        {
            get { return minutesChkPnL; }
            set { minutesChkPnL = Math.Max(-1, value); }
        }


        public int BarsHoldEnOrd
        {
            get { return barsHoldEnOrd; }
            set { barsHoldEnOrd = Math.Max(1, value); }
        }


        public int EnCounterPullBackBars
        {
            get { return enCounterPBBars; }
            set { enCounterPBBars = Math.Max(-1, value); }
        }


        public int BarsSincePtSl
        {
            get { return barsSincePtSl; }
            set { barsSincePtSl = Math.Max(1, value); }
        }


        public int BarsToCheckPL
        {
            get { return barsToCheckPL; }
            set { barsToCheckPL = Math.Max(1, value); }
        }

        public double EnSwingMinPnts
        {
            get { return enSwingMinPnts; }
            set { enSwingMinPnts = Math.Max(1, value); }
        }


        public double EnSwingMaxPnts
        {
            get { return enSwingMaxPnts; }
            set { enSwingMaxPnts = Math.Max(4, value); }
        }


        public double EnPullbackMinPnts
        {
            get { return enPullbackMinPnts; }
            set { enPullbackMinPnts = Math.Max(1, value); }
        }


        public double EnPullbackMaxPnts
        {
            get { return enPullbackMaxPnts; }
            set { enPullbackMaxPnts = Math.Max(2, value); }
        }


        public double EnOffsetPnts
        {
            get { return enOffsetPnts; }
            set { enOffsetPnts = Math.Max(0, value); }
        }

        //        [Description("Offeset points for limit price entry, pullback entry")]
        //        [GridCategory("Parameters")]
        //        public double EnOffset2Pnts
        //        {
        //            get { return enOffset2Pnts; }
        //            set { enOffset2Pnts = Math.Max(0, value); }
        //        }


        public bool EnTrailing
        {
            get { return enTrailing; }
            set { enTrailing = value; }
        }


        public bool PTTrailing
        {
            get { return ptTrailing; }
            set { ptTrailing = value; }
        }


        public bool SLTrailing
        {
            get { return slTrailing; }
            set { slTrailing = value; }
        }


        public int TradeDirection
        {
            get { return tradeDirection; }
            set { tradeDirection = value; }
        }


        public int TradeStyle
        {
            get { return tradeStyle; }
            set { tradeStyle = value; }
        }


        public bool BackTest
        {
            get { return backTest; }
            set { backTest = value; }
        }


        public int PrintOut
        {
            get { return printOut; }
            set { printOut = Math.Max(-1, value); }
        }

        #endregion
    }
}