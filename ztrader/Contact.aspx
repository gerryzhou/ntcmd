﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="ztrader.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <asp:Label ID="Label1" runat="server" Text="AlgoMode:"></asp:Label><asp:TextBox ID="TextBox1" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label18" runat="server" Text="RetracePnts:"></asp:Label><asp:TextBox ID="TextBox18" runat="server" Text="6"></asp:TextBox>
    <br />
    <asp:Label ID="Label19" runat="server" Text="ProfitTargetAmt:"></asp:Label><asp:TextBox ID="TextBox19" runat="server" Text="400"></asp:TextBox>
    <br />
    <asp:Label ID="Label20" runat="server" Text="ProfitTgtIncTic:"></asp:Label><asp:TextBox ID="TextBox20" runat="server" Text="8"></asp:TextBox>
    <br />
    <asp:Label ID="Label21" runat="server" Text="ProfitLockMinTic:"></asp:Label><asp:TextBox ID="TextBox21" runat="server" Text="16"></asp:TextBox>
    <br />
    <asp:Label ID="Label22" runat="server" Text="ProfitLockMaxTic:"></asp:Label><asp:TextBox ID="TextBox22" runat="server" Text="30"></asp:TextBox>
    <br />
    <asp:Label ID="Label23" runat="server" Text="StopLossAmt:200:"></asp:Label><asp:TextBox ID="TextBox23" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label24" runat="server" Text="TrailingStopLossAmt:200:"></asp:Label><asp:TextBox ID="TextBox24" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label25" runat="server" Text="StopLossIncTic:8:"></asp:Label><asp:TextBox ID="TextBox25" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label26" runat="server" Text="BreakEvenAmt:150:"></asp:Label><asp:TextBox ID="TextBox26" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label27" runat="server" Text="DailyLossLmt:-200:"></asp:Label><asp:TextBox ID="TextBox27" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label28" runat="server" Text="TimeStart:90200:"></asp:Label><asp:TextBox ID="TextBox28" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label29" runat="server" Text="TimeEnd:105500:"></asp:Label><asp:TextBox ID="TextBox29" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label30" runat="server" Text="MinutesChkEnOrder:10:"></asp:Label><asp:TextBox ID="TextBox30" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label31" runat="server" Text="MinutesChkPnL:30:"></asp:Label><asp:TextBox ID="TextBox31" runat="server" Text="1"></asp:TextBox>
    <br />    
    <asp:Label ID="Label2" runat="server" Text="BarsHoldEnOrd:2:"></asp:Label><asp:TextBox ID="TextBox2" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label3" runat="server" Text="EnCounterPullBackBars:2:"></asp:Label><asp:TextBox ID="TextBox3" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label4" runat="server" Text="BarsSincePtSl:1:"></asp:Label><asp:TextBox ID="TextBox4" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label5" runat="server" Text="BarsToCheckPL:2:"></asp:Label><asp:TextBox ID="TextBox5" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label6" runat="server" Text="EnSwingMinPnts:11:"></asp:Label><asp:TextBox ID="TextBox6" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label7" runat="server" Text="EnSwingMaxPnts:16:"></asp:Label><asp:TextBox ID="TextBox7" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label8" runat="server" Text="EnPullbackMinPnts:5:"></asp:Label><asp:TextBox ID="TextBox8" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label9" runat="server" Text="EnPullbackMaxPnts:9:"></asp:Label><asp:TextBox ID="TextBox9" runat="server" Text="1"></asp:TextBox>
    <br />  
    <asp:Label ID="Label10" runat="server" Text="EnOffsetPnts:0.75:"></asp:Label><asp:TextBox ID="TextBox10" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label11" runat="server" Text="EnTrailing:true:"></asp:Label><asp:TextBox ID="TextBox11" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label12" runat="server" Text="PTTrailing:true:"></asp:Label><asp:TextBox ID="TextBox12" runat="server" Text="1"></asp:TextBox>
    <br />  
    <asp:Label ID="Label13" runat="server" Text="SLTrailing:true:"></asp:Label><asp:TextBox ID="TextBox13" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label14" runat="server" Text="TradeDirection:-1:"></asp:Label><asp:TextBox ID="TextBox14" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label15" runat="server" Text="TradeStyle:-1:"></asp:Label><asp:TextBox ID="TextBox15" runat="server" Text="1"></asp:TextBox>
    <br />  
    <asp:Label ID="Label16" runat="server" Text="BackTest:false:"></asp:Label><asp:TextBox ID="TextBox16" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label17" runat="server" Text="PrintOut:1:"></asp:Label><asp:TextBox ID="TextBox17" runat="server" Text="1"></asp:TextBox>
    <br />
    <asp:Label ID="Label32" runat="server" Text="AlgoMode:"></asp:Label><asp:TextBox ID="TextBox32" runat="server" Text="1"></asp:TextBox>
    <br />  

    <asp:Panel ID="Panel1" runat="server">

    </asp:Panel>
        <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Horizontal">
            <asp:ListItem Text="1" Value="1"></asp:ListItem><asp:ListItem Text="2" Value="2"></asp:ListItem>            
        </asp:CheckBoxList>
    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" ToolTip="Submit the form" />
    <asp:Button ID="Button4" runat="server" Text="Button" />
    <asp:CheckBox ID="CheckBox3" runat="server" />  
    <asp:CheckBox ID="CheckBox4" runat="server" />
    <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem Text="rd1" Value="1"></asp:ListItem>      
        <asp:ListItem Text="rd2" Value="2"></asp:ListItem>
    </asp:RadioButtonList>
    <asp:RadioButton ID="RadioButton3" runat="server" />
    <asp:RadioButton ID="RadioButton4" runat="server" />
    <asp:ImageButton ID="ImageButton2" Width="100" Height="30" runat="server" />

    <asp:Button ID="Button1" runat="server" Text="Button" />
    <asp:Button ID="Button2" runat="server" Text="Button" />
    <asp:CheckBox ID="CheckBox1" runat="server" />  
    <asp:CheckBox ID="CheckBox2" runat="server" />
    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
        <asp:ListItem Text="rd1" Value="1"></asp:ListItem>
        <asp:ListItem Text="rd2" Value="2"></asp:ListItem>
    </asp:RadioButtonList>
    <asp:RadioButton ID="RadioButton1" runat="server" />
    <asp:RadioButton ID="RadioButton2" runat="server" />
    <asp:ImageButton ID="ImageButton1" Width="100" Height="30" runat="server" />

    <h3>Your contact page.
    </h3>
    <address>
        One Microsoft Way<br />
        Redmond, WA 98052-6399
        <br />
        <abbr title="Phone">P:</abbr>
        425.555.0100
    </address>
    <address>
        <strong>Support:</strong>   <a href="mailto:Support@example.com">Support@example.com</a>
        <br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
    </address>
</asp:Content>
