﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ztrader
{
    public partial class ZtLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Btn_Log_Click(object sender, EventArgs e)
        {
            string accName = RBL_AccNum.SelectedValue.ToString();
            string path = @"C:\inetpub\wwwroot\nt_files\log\" + accName + Path.DirectorySeparatorChar + DDListLogFile.SelectedValue;
            Tbx_LogText.Text = string.Empty;
            Tbx_LogText.Text += "Show log file->" + path + Environment.NewLine;
            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    Tbx_LogText.Text += "Show log for:" + sr.ReadLine() + Environment.NewLine;
                }
            }
        }

        protected void DDListLogFile_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void RBL_AccNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            string accName = RBL_AccNum.SelectedValue.ToString();
            DDListLogFile.Items.Clear();
            Tbx_LogText.Text = string.Empty;
            Tbx_LogText.Text += "Show log for Show log for:" + accName + Environment.NewLine;
            FileInfo[] cmdFiles = Utils.GetCmdFile(@"C:\inetpub\wwwroot\nt_files\log\" + accName + Path.DirectorySeparatorChar);
            foreach (FileInfo f in cmdFiles)
            {
                DDListLogFile.Items.Add(f.Name);
                Tbx_LogText.Text += "Show log for:" + f.Name + Environment.NewLine;
            }
        }

        protected void Tbx_LogText_TextChanged(object sender, EventArgs e)
        {
            //Tbx_LogText.SelectionStart = Tbx_LogText.Text.Length - 1;
            //Tbx_LogText.ScrollToCaret();
        }
    }
}