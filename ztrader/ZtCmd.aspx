﻿<%@ Page Title="ZTCmd" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ZtCmd.aspx.cs" Inherits="ztrader.ZtCmd" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
        <div>
        </div>
    <asp:Panel ID="Panel1" runat="server">
        <asp:RadioButtonList ID="RBL_AccNum" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RBL_AccNum_SelectedIndexChanged">
            <asp:ListItem>M8175</asp:ListItem>
            <asp:ListItem>Sim101</asp:ListItem>
            <asp:ListItem>Sim102</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Button ID="Btn_Cmd" runat="server" OnClick="Btn_Cmd_Click" Text="SubmitCmd" />
    </asp:Panel>
    <asp:Panel ID="Pnl_Cmd" runat="server">
        <asp:Label ID="Label1" runat="server" Text="AlgoMode:"></asp:Label><asp:TextBox ID="Tbx_AlgoMode" Columns="1" runat="server" Text="1"></asp:TextBox>
        
        <asp:Label ID="Label18" runat="server" Text="RetracePnts:"></asp:Label><asp:TextBox ID="Tbx_RetracePnts" runat="server" Columns="2" Text="6"></asp:TextBox>
        <br />
        <asp:Label ID="Label19" runat="server" Text="ProfitTargetAmt:"></asp:Label><asp:TextBox ID="Tbx_ProfitTargetAmt" runat="server" Columns="4" Text="400"></asp:TextBox>
        
        <asp:Label ID="Label20" runat="server" Text="ProfitTgtIncTic:"></asp:Label><asp:TextBox ID="Tbx_ProfitTgtIncTic" runat="server" Columns="2" Text="8"></asp:TextBox>
        <br />        
        <asp:Label ID="Label21" runat="server" Text="ProfitLockMinTic:"></asp:Label><asp:TextBox ID="Tbx_ProfitLockMinTic" runat="server" Columns="2" Text="16"></asp:TextBox>
        
        <asp:Label ID="Label22" runat="server" Text="ProfitLockMaxTic:"></asp:Label><asp:TextBox ID="Tbx_ProfitLockMaxTic" runat="server" Columns="2" Text="30"></asp:TextBox>
        <br />
        <asp:Label ID="Label23" runat="server" Text="StopLossAmt:"></asp:Label><asp:TextBox ID="Tbx_StopLossAmt" runat="server"  Columns="3" Text="200"></asp:TextBox>
        
        <asp:Label ID="Label24" runat="server" Text="TrailingStopLossAmt:"></asp:Label><asp:TextBox ID="Tbx_TrailingStopLossAmt" runat="server" Columns="3" Text="200"></asp:TextBox>
        <br />
        <asp:Label ID="Label25" runat="server" Text="StopLossIncTic:"></asp:Label><asp:TextBox ID="Tbx_StopLossIncTic" runat="server" Columns="2" Text="8"></asp:TextBox>
        <br />
        <asp:Label ID="Label26" runat="server" Text="BreakEvenAmt:"></asp:Label><asp:TextBox ID="Tbx_BreakEvenAmt" runat="server" Columns="3" Text="150"></asp:TextBox>
        
        <asp:Label ID="Label27" runat="server" Text="DailyLossLmt:"></asp:Label><asp:TextBox ID="Tbx_DailyLossLmt" runat="server" Columns="5" Text="-200"></asp:TextBox>
        <br />
        <asp:Label ID="Label28" runat="server" Text="TimeStart:"></asp:Label><asp:TextBox ID="Tbx_TimeStart" runat="server" Columns="6" Text="90200"></asp:TextBox>
        
        <asp:Label ID="Label29" runat="server" Text="TimeEnd:"></asp:Label><asp:TextBox ID="Tbx_TimeEnd" runat="server" Columns="6" Text="105500"></asp:TextBox>
        <br />
        <asp:Label ID="Label30" runat="server" Text="MinutesChkEnOrder:"></asp:Label><asp:TextBox ID="Tbx_MinutesChkEnOrder" runat="server" Columns="2" Text="10"></asp:TextBox>
        
        <asp:Label ID="Label31" runat="server" Text="MinutesChkPnL:"></asp:Label><asp:TextBox ID="Tbx_MinutesChkPnL" runat="server" Columns="2" Text="30"></asp:TextBox>
        <br />
        <asp:Label ID="Label2" runat="server" Text="BarsHoldEnOrd:"></asp:Label><asp:TextBox ID="Tbx_BarsHoldEnOrd" runat="server" Columns="2" Text="2"></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="EnCounterPullBackBars:"></asp:Label><asp:TextBox ID="Tbx_EnCounterPullBackBars" runat="server" Columns="2" Text="2"></asp:TextBox>
        <br />
        <asp:Label ID="Label4" runat="server" Text="BarsSincePtSl:"></asp:Label><asp:TextBox ID="Tbx_BarsSincePtSl" runat="server" Columns="2" Text="1"></asp:TextBox>
        <br />
        <asp:Label ID="Label5" runat="server" Text="BarsToCheckPL:"></asp:Label><asp:TextBox ID="Tbx_BarsToCheckPL" runat="server" Columns="2" Text="2"></asp:TextBox>
        <br />
        <asp:Label ID="Label6" runat="server" Text="EnSwingMinPnts:"></asp:Label><asp:TextBox ID="Tbx_EnSwingMinPnts" runat="server" Columns="2" Text="11"></asp:TextBox>
        
        <asp:Label ID="Label7" runat="server" Text="EnSwingMaxPnts:"></asp:Label><asp:TextBox ID="Tbx_EnSwingMaxPnts" runat="server" Columns="3" Text="16"></asp:TextBox>
        <br />
        <asp:Label ID="Label8" runat="server" Text="EnPullbackMinPnts:"></asp:Label><asp:TextBox ID="Tbx_EnPullbackMinPnts" runat="server" Columns="2" Text="5"></asp:TextBox>
        
        <asp:Label ID="Label9" runat="server" Text="EnPullbackMaxPnts:"></asp:Label><asp:TextBox ID="Tbx_EnPullbackMaxPnts" runat="server" Columns="3" Text="9"></asp:TextBox>
        <br />
        <asp:Label ID="Label10" runat="server" Text="EnOffsetPnts:"></asp:Label><asp:TextBox ID="Tbx_EnOffsetPnts" runat="server" Columns="4" Text="0.75"></asp:TextBox>
        <br />
        <asp:Label ID="Label11" runat="server" Text="EnTrailing:"></asp:Label><asp:TextBox ID="Tbx_EnTrailing" runat="server" Columns="5" Text="true"></asp:TextBox>
        <br />
        <asp:Label ID="Label12" runat="server" Text="PTTrailing:"></asp:Label><asp:TextBox ID="Tbx_PTTrailing" runat="server" Columns="5" Text="true"></asp:TextBox>
        <br />
        <asp:Label ID="Label13" runat="server" Text="SLTrailing:"></asp:Label><asp:TextBox ID="Tbx_SLTrailing" runat="server" Columns="5" Text="true"></asp:TextBox>
        <br />
        <asp:Label ID="Label14" runat="server" Text="TradeDirection:"></asp:Label><asp:TextBox ID="Tbx_TradeDirection" runat="server" Columns="2" Text="-1"></asp:TextBox>
        
        <asp:Label ID="Label15" runat="server" Text="TradeStyle:"></asp:Label><asp:TextBox ID="Tbx_TradeStyle" runat="server" Columns="2" Text="-1"></asp:TextBox>
        <br />
        <asp:Label ID="Label16" runat="server" Text="BackTest:"></asp:Label><asp:TextBox ID="Tbx_BackTest" runat="server" Columns="5" Text="false"></asp:TextBox>
        
        <asp:Label ID="Label17" runat="server" Text="PrintOut:"></asp:Label><asp:TextBox ID="Tbx_PrintOut" runat="server" Columns="2" Text="1"></asp:TextBox>
        <br />
    </asp:Panel>
    <asp:TextBox runat="server" ID="Tbx_Log" TextMode="MultiLine" Rows="40" style="min-width: 100%" Wrap="False"></asp:TextBox>
</asp:Content>
