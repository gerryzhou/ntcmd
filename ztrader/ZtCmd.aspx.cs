﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ztrader
{
    public partial class ZtCmd : System.Web.UI.Page
    {
        //@"C:\inetpub\wwwroot\nt_files\log\" + accName + Path.DirectorySeparatorChar
        private string cmdPath = Utils.WEBROOTPATH + "cmd" + Path.DirectorySeparatorChar;// @"C:\inetpub\wwwroot\nt_files\cmd";
        private string cmdBakPath = Utils.WEBROOTPATH + "cmd_bak" + Path.DirectorySeparatorChar;
        private Dictionary<string, string> paraObjs;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_Cmd_Click(object sender, EventArgs e)
        {
            LoadParams();
            string cmdFile = Utils.GetFileNameByDateTime(DateTime.Now, cmdPath, RBL_AccNum.SelectedValue, "cmd");
            Utils.PutCmdFile(paraObjs, cmdFile, "");
           
        }

        protected void RBL_AccNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmdPath = updateCmdPath("cmd", RBL_AccNum.SelectedValue);
            cmdBakPath = updateCmdPath("cmd_bak", RBL_AccNum.SelectedValue);
        }

        protected void LoadParams()
        {
            paraObjs = new Dictionary<string, string>();
            Type t = typeof(GSZigZag);
            foreach(PropertyInfo pi in t.GetProperties())
            {
                TextBox tb = (TextBox)Pnl_Cmd.FindControl("Tbx_" + pi.Name);
                string val = tb==null? "-99":tb.Text;
                Tbx_Log.Text += "Tbx_" + pi.Name + ":" + val + Environment.NewLine;
                paraObjs.Add(pi.Name, val);
            }           
            
        }

        protected string updateCmdPath(string cmd_type, string acc_name)
        {
            return Utils.WEBROOTPATH + cmd_type + Path.DirectorySeparatorChar + acc_name + Path.DirectorySeparatorChar;
        }
    }
}