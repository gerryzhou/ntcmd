﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ZtLog.aspx.cs" Inherits="ztrader.ZtLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="Panel1" runat="server">

        <asp:RadioButtonList ID="RBL_AccNum" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RBL_AccNum_SelectedIndexChanged">
            <asp:ListItem>M8175</asp:ListItem>
            <asp:ListItem>Sim101</asp:ListItem>
            <asp:ListItem>Sim102</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Button ID="Btn_Log" runat="server" OnClick="Btn_Log_Click" Text="ShowLog" /><asp:DropDownList ID="DDListLogFile" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDListLogFile_SelectedIndexChanged"></asp:DropDownList>
    </asp:Panel>
    <input id="Btn_ScroolBtm" onclick="textBoxScrollBtm('<%=Tbx_LogText.ClientID %>')" type="button" value="ScrollBottom" /><input id="Btn_ScroolUp" onclick="textBoxScrollUp('<%=Tbx_LogText.ClientID %>')" type="button" value="ScrollUp" />
<asp:TextBox ID="Tbx_LogText" runat="server" TextMode="MultiLine" Rows="40" style="min-width: 100%" Wrap="False" OnTextChanged="Tbx_LogText_TextChanged"></asp:TextBox>

</asp:Content>
