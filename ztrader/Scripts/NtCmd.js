var textBoxCurVerticalPos = 0;
function textBoxScrollBtm(tbID) {

    var objDiv = document.getElementById(tbID);

    objDiv.scrollTop = objDiv.scrollHeight;

}

function textBoxScrollUp(tbID) {

    var objDiv = document.getElementById(tbID);
    textBoxCurVerticalPos = objDiv.scrollTop - objDiv.scrollHeight / 5
    objDiv.scrollTop = textBoxCurVerticalPos > 0 ? textBoxCurVerticalPos:0;
}